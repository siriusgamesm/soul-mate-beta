// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "SoulMateBeta.h"
#include "SoulMateBetaGameMode.h"
#include "SoulMateBetaHUD.h"
#include "SoulMateBetaCharacter.h"

ASoulMateBetaGameMode::ASoulMateBetaGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASoulMateBetaHUD::StaticClass();
}
