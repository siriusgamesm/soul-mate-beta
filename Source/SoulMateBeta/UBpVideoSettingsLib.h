// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "UBpVideoSettingsLib.generated.h"

/**
 * 
 */
UCLASS()
class SOULMATEBETA_API UUBpVideoSettingsLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	

	public:
	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static bool SetScreenResolution(const int32 Width, const int32 Height, const bool Fullscreen);

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static bool ChangeScreenResolution(const int32 Width, const int32 Height, const bool Fullscreen);

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static bool GetSupportedScreenResolutions(TArray<FString>& Resolutions);

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static bool GetVideoQualitySettings(int32& AntiAliasing, int32& Effects, int32& PostProcess,
	int32& Resolution, int32& Shadow, int32& Texture, int32& ViewDistance);

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static bool SetVideoQualitySettings(const int32 AntiAliasing, const int32 Effects, const int32 PostProcess,
	const int32 Resolution, const int32 Shadow, const int32 Texture, const int32 ViewDistance);

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static bool SaveVideoModeAndQuality();

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static FString GetWidth(FString string);

	UFUNCTION(BlueprintCallable, Category = "Video Setting Beta")
	static FString GetLength(FString string);
	
	private:
	// Try to get the GameUserSettings object from the engine
	static UGameUserSettings* GetGameUserSettings();
	
	
};
